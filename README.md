# Wstęp do uczenia maszynowego w języku Python

### Wstęp

Repozytorium zawiera dane oraz przykładowe notebooki dla szkolenia DSC, które zostało zorganizowane w trakcie Digital WKS 2019.

Na projekt składają się następujące elementy:
- data
  - cancer.data - dane do notebooka *introduction.ipynb*
  - train_set.csv - zbiór danych oznaczonych z przykładowym rozwiązaniem w notebooku *heart_disease_example.ipynb*
  - test_set.csv - zbiór danych nieoznaczonych z przykładowym rozwiązaniem w notebooku *heart_disease_example.ipynb*
- src - zawiera uniwersalny kod
- notebook - zawiera Jupyter notebooki
  - *introduction.ipynb*  - wstęp wprowadzający do języka Python oraz pokazujący podstawowy scenariusz uczenia modelu
  - *heart_disease_example.ipynb*  - zbuduje przykładowy model i wykona predykcje dla problemu heart_disease


### Instalacja środowiska

Projekt można uruchomić na dwa sposoby.

**Anaconda**

Ten sposób pozwoli ci na zainstalowanie zintegrowanego środowiska. Jest polecany dla osób, które mają nie mają doświadczenia z tego typu projektami.

1. Pobierz Anaconda Python 3.7: [https://repo.anaconda.com/archive/Anaconda3-2019.03-Windows-x86_64.exe](https://repo.anaconda.com/archive/Anaconda3-2019.03-Windows-x86_64.exe) (Link jest do wersji Windows 64 bit). Jest to platforma do projektów data science. W środku jest m.in. Python oraz Jupyter.
2. Zainstaluj Anaconda używając domyślnych ustawień.
3. Pobierz branch master z repozytorium do wybranej lokaalizacji w katalogu użytkownika.
4. Otwórz Jupyter Notebook, który został zainstalowany wraz z Anaconda.
5. Jupyter otworzy się automatycznie w Twojej domyślnej przeglądarce. Jeżeli tak się nie stało otwórz adres localhost:8888.
6. Otwórz wybrany notebook `ścieżka_repo/notebook/*.ipynb` w Jupyter.

**Pip**

Ten sposób pozwoli Ci na instalację poszczególnych składników i na bardziej granularną kontrolę na swoim środowiskiem. Zalecany dla użytkowników z doświadczeniem z Python.

1. Zainstaluj Python 3.7.
2. Pobierz branch master z repozytorium do wybranej lokaalizacji w katalogu użytkownika.
3. Stwórz wirtualne środowisko w wybranej lokalizacji - `python -m venv env`.
4. Aktywuj środowisko `env/Scripts/activate.bat`.
5. Zainstaluj zależności projektu `pip install -r requirements.txt`.
6. Uruchom Jupyter - `jupyter notebook`.
7. Otwórz wybrany notebook `ścieżka_repo/notebook/*.ipynb` w Jupyter.
 
### Materiały

- [Seria na YT o analizie danych](https://www.youtube.com/playlist?list=PLQVvvaa0QuDfSfqQuee6K8opKtZsh7sA9)
- [Seria na YT o uczeniu maszynowym](https://www.youtube.com/playlist?list=PLQVvvaa0QuDfKTOs3Keq_kaG2P55YRn5v)
- [Kurs Stanford ML](https://www.coursera.org/learn/machine-learning)
- [User guide to jednego z podstawowych MLowych narzędzi. Zawiera opisy dostępnych narzędzi w tym modeli, narzędzi do analizy oraz przetwarzania danych](https://scikit-learn.org/stable/user_guide.html)
- [Najpopularniejsza storna z konkursami ML](https://www.kaggle.com/)

