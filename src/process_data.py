import pandas as pd
import numpy as np

def save_model_predictions_to_file(filename, model, patientid_values, data):
    predictions = model.predict(data)
    result_df = pd.DataFrame()
    result_df["patientid"] = patientid_values
    result_df["hd"] = predictions
    result_df.to_csv(filename, sep=',', index=False)